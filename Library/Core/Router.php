<?php

namespace Library\Core;

use Library\Utilities\Request;

class Router {
    private $routes = [];

    public function get(string $uri, $callback)
    {
        $this->routes['GET'][$uri] = $callback;
    }

    public function post(string $uri, $callback)
    {
        $this->routes['POST'][$uri] = $callback;
    }

    public function __destruct()
    {
        $clean_uri = explode("?", $_SERVER["REQUEST_URI"])[0];
        $route_callback = $this->routes[$_SERVER["REQUEST_METHOD"]][$clean_uri];

        if ($route_callback) {
            if (is_array($route_callback)) {
                $controller = new $route_callback[0];
                call_user_func_array([$controller, $route_callback[1]], [new Request()]);
            } else {
                $route_callback(new Request());
            }
        }
    }
}
