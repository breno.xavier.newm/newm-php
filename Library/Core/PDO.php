<?php

namespace Library\Core;

use PDO as Database;

class PDO
{
    private static $pdo;

    public static function connect()
    {
        if (is_null(self::$pdo)) {
            self::$pdo = new Database("mysql:host=database;port=3306;dbname=newm", "root", "newm");
        }

        return self::$pdo;
    }
}