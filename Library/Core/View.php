<?php

namespace Library\Core;

class View
{
    private string $view_path;
    private ?array $view_data;

    public function __construct(string $view, array $data = null)
    {
        $this->view_path = $view;
        $this->view_data = $data;
    }

    public function __destruct()
    {
        $path = __DIR__ . '/../../App/Views/' . $this->view_path . '.view.php';

        if (is_file($path)) {
            $data = $this->view_data;
            require $path;
        }
    }
}
