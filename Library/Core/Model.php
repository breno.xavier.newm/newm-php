<?php

namespace Library\Core;

class Model
{
    protected string $table;

    /**
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }
}
