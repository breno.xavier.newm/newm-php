<?php

namespace Library\Core;

use Library\Utilities\SQL;

class Database
{
    public static function find(): array
    {

    }

    public static function add(object $model): array
    {
        $sql = new SQL($model);
        $sql_insert = $sql->buildInsert();
        $sql_data = $sql->buildDataArray();

        $pdo = PDO::connect();
        $db = $pdo->prepare($sql_insert);
        $db->execute($sql_data);
        return $db->fetchAll(\PDO::FETCH_ASSOC);
    }
}