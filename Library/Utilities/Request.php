<?php

namespace Library\Utilities;

class Request {
    public function redirect($url): void
    {
        header("Location: $url");
    }

    public function input(string $name): string
    {
        if (isset($_GET[$name])) {
            return $_GET[$name];
        } else if (isset($_POST[$name])) {
            return $_POST[$name];
        }

        return "";
    }

    public function only(array $names): array
    {
        $body = [];

        foreach ($names as $name) {
            if (isset($_GET[$name])) {
                $body[$name] = $_GET[$name];
            } else if (isset($_POST[$name])) {
                $body[$name] = $_POST[$name];
            }
        }

        return $body;
    }
}
