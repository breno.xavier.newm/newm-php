<?php


namespace Library\Utilities;


class SQL
{
    private object $model;
    private array $vars;
    private array $keys;

    public function __construct(object $model)
    {
        $this->model = $model;
        $this->vars = get_object_vars($this->model);
        $this->keys = array_keys($this->vars);
    }

    public function buildInsert(): string
    {
        $sql = "INSERT INTO {$this->model->getTable()} (";

        foreach ($this->keys as $i => $key) {
            if ((count($this->keys) - 1) == $i) {
                $sql .= "$key, created_at, updated_at) VALUES (";
                continue;
            }

            $sql .= "$key, ";
        }

        foreach ($this->keys as $i => $key) {
            if ((count($this->keys) - 1) == $i) {
                $sql .= ":$key, :created_at, :updated_at) RETURNING *;";
                continue;
            }

            $sql .= ":$key, ";
        }

        return $sql;
    }

    public function buildDataArray(): array
    {
        $data = [
            ':created_at' => date("Y-m-d H:i:s"),
            ':updated_at' => date("Y-m-d H:i:s")
        ];

        foreach ($this->keys as $key) {
            $data[":$key"] = $this->model->{$key};
        }

        return $data;
    }
}