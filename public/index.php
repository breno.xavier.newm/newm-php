<?php

error_reporting(E_ALL);
ini_set('display_errors', true);
header('Content-Type: text/html; charset=utf-8');

require __DIR__ . '/../Library/Core/AutoLoad.php';
AutoLoad::load();

require __DIR__ . '/../App/routes.php';
