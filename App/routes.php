<?php

use App\Controllers\UserController;
use Library\Core\Router;
use Library\Utilities\Request;

$router = new Router();

$router->get('/', function (Request $request) {
    $request->redirect("/user");
});

$router->get('/user', [UserController::class, "index"]);
$router->post('/user', [UserController::class, "save"]);
