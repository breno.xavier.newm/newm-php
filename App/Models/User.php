<?php

namespace App\Models;

use DateTime;
use Library\Core\Model;

class User extends Model
{
    public int $id;
    public string $name;
    public string $email;
    public string $password;
    public DateTime $created_at;
    public DateTime $updated_at;

    public function __construct()
    {
        $this->table = "users";
    }
}
