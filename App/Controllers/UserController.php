<?php

namespace App\Controllers;

use App\Models\User;
use Library\Core\Database;
use Library\Core\View;
use Library\Utilities\Request;

class UserController
{
    public function index()
    {
        return new View("user/index");
    }

    public function save(Request $request): View
    {
        $new_user = new User();
        $new_user->name = $request->input("name");
        $new_user->email = $request->input("email");
        $new_user->password = $request->input("name");

        $new_user = Database::add($new_user);

        return new View("user/saved", $new_user);
    }
}
